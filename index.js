// Users Collection (users.json)
{
    "_id": "admin0001",
    "firstName": "Felipe",
    "lastName": "Mandadamot",
    "email": "felipemandadamot@email.com",
    "password": "mandadamot123",
    "isAdmin": true,
    "mobileNumber": "1234567890",
    "dateTimeRegistered": "2022-06-10T15:00:00.00Z"
},

{
    "_id": "user0001",
    "firstName": "Solomon",
    "lastName": "Mandurukot",
    "email": "solomonmandurukot@email.com",
    "password": "mandurukot123",
    "isAdmin": true,
    "mobileNumber": "4556456456",
    "dateTimeRegistered": "2022-06-10T15:00:00.00Z"
},

{
    "_id": "user0002",
    "firstName": "Magdalena",
    "lastName": "Umasa",
    "email": "magdalenaumasa@email.com",
    "password": "umasa123",
    "isAdmin": true,
    "mobileNumber": "7897897899",
    "dateTimeRegistered": "2022-06-10T15:00:00.00Z"
}

// --------------------------
// Products Collection (products.json)

{
    "_id": "product001",
    "name": "laptop",
    "description": "device",
    "price": "1000",
    "stocks": 50,
    "isActive": true,
    "SKU": "00234-MBDGT"
}, 

{
    "_id": "product002",
    "name": "skateboard",
    "description": "for skating",
    "price": "500",
    "stocks": 50,
    "isActive": true,
    "SKU": "00234-MBDGGG"
}

// -----------------------------
// Order Products Collection (orderProducts.json)

{
    "_id": "orderproducts001",
    "orderId": "1234",
    "productId": ["product002"],
    "quantity": [1],
    "price": 500,
    "subTotal": 500
}

// -----------------------------
// Orders (orders.json)

{
    "_id": "orderId001",
    "userId": "user0002",
    "transactionDate": "2022-06-10T15:00:00.00Z",
    "status": "pending",
    "total": 500
}